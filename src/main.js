import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
// View Components.
import Albums from './views/Albums';
import AlbumDetail from './views/AlbumDetail';
import Profile from './views/Profile';

Vue.use( VueRouter );

const routes = [
  {
    path: '/albums',
    name: 'Albums',
    component: Albums
  },
  {
    path: '/albums/:albumId',
    name: 'AlbumDetail',
    component: AlbumDetail
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/',
    redirect: '/albums'
  }
];

const router = new VueRouter({ routes });

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
