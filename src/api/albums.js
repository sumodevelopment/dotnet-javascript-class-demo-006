export const getAllAlbums = () => {
    return fetch('http://localhost:3000/v1/albums')
        .then(resp => resp.json())
        .then(resp => {
            if (resp.success === false) {
                throw Error(resp.error);
            }
            return resp;
        })
        .then(resp => resp.data)
};

export const getAlbum = id => {
    return fetch(`http://localhost:3000/v1/albums/${id}`)
        .then(resp => resp.json())
        .then(resp => {
            if (resp.success === false) {
                throw Error(resp.error);
            }
            return resp;
        })
        .then(resp => resp.data)
};

export const getAlbumTracks = albumId => {
    return fetch(`http://localhost:3000/v1/tracks/${albumId}`)
        .then(resp => resp.json())
        .then(resp => {
            if (resp.success === false) {
                throw Error(resp.error);
            }
            return resp;
        })
        .then(resp => resp.data)
};
