# class-demo-006

## Planning our app
* [x] Install VueRouter
* [x] Choose/Create Views [ AlbumList, AlbumDetail, Profile ]
* [ ] Breakdown Views to Components [ ]
* [ ] Communicate with a Backend (your favourite)
* [ ] Integrate Components Views and with Backend 
* [ ] If theres time! - VueX


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
